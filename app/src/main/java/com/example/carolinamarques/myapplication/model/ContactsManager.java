package com.example.carolinamarques.myapplication.model;
import java.util.LinkedList;

public enum ContactsManager {
    INSTANCE;

    private LinkedList<Contact> contacts;

    ContactsManager() {
        this.contacts = new LinkedList<>();
        //setupInitialContacts();
    }

    private void setupInitialContacts(){
        contacts.add(new Contact("Catarina Reis"));
        contacts.add(new Contact("Isabel Ferreira"));
        //contacts.add(new Contact("James Houghton", 31, "76.9", "married"));
    }

    public void add(Contact contact){
        contacts.add(contact);
    }

    public void remove(Contact contact){
        contacts.remove(contact);
    }

    public LinkedList<Contact> getContacts(){
        return new LinkedList<>(contacts);
    }

    public void clearAllContacts(){
        contacts = new LinkedList<>();
    }

    public boolean hasContacts(){
        return !contacts.isEmpty();
    }

    public Contact getContact(int contactPosition) {
        return contacts.get(contactPosition);
    }
}
