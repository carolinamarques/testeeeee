package com.example.carolinamarques.myapplication;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private String username = "USER_1";
    private String password = "XPTO_1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void onClickDoLogin(View view) {

        EditText inputUsername = findViewById(R.id.editTextUsername);
        EditText inputPassword = findViewById(R.id.editTextPassword);

        if (!(username.equals(inputUsername.getText().toString()) && password.equals(inputPassword.getText().toString()))){
            showErrorMessage(R.string.invalidcredentials);
            return;
        }

        startActivity(ContactsListActivity.getIntent(this));
    }


    private void showErrorMessage(int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(message);

        builder.setNeutralButton(R.string.OK, null);

        builder.show();
    }
}
