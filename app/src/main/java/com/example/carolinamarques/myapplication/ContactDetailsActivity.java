package com.example.carolinamarques.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.carolinamarques.myapplication.model.Contact;
import com.example.carolinamarques.myapplication.model.ContactsManager;

public class ContactDetailsActivity extends AppCompatActivity {

    private static final String CONTACTPOSITION = "CONTACTPOSITION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        int contactPosition = getIntent().getIntExtra(CONTACTPOSITION, -1);

        Contact contact = ContactsManager.INSTANCE.getContact(contactPosition);

        setTitle(getString(R.string.contactdetails) + " " + contact.getName());

        TextView textView = findViewById(R.id.textViewName);
        textView.setText(contact.getName());

        textView = findViewById(R.id.textViewAge);
        textView.setText(contact.getAge() + "");

        textView = findViewById(R.id.textViewWeight);
        textView.setText(contact.getWeight() + "");

        textView = findViewById(R.id.textViewStatus);
        textView.setText(contact.getStatus());
    }

    public static Intent getIntent(Context context, int contactPosition) {
        Intent intent = new Intent(context, ContactDetailsActivity.class);
        intent.putExtra(CONTACTPOSITION, contactPosition);
        return intent;
    }

}
