package com.example.carolinamarques.myapplication.model;

public class Contact {
    private String name;
    private int age;
    private String weight;
    private String status;

    public Contact(String name) {
        this.name = name;
    }

    public Contact(String name, int age, String weight, String status) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getWeight() {
        return weight;
    }

    public String getStatus() {
        return status;
    }
}
