package com.example.carolinamarques.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.carolinamarques.myapplication.model.Contact;
import com.example.carolinamarques.myapplication.model.ContactsManager;

public class ContactsListActivity extends AppCompatActivity {

    private ArrayAdapter<Contact> adapter;
    private ListView listViewContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);

        listViewContacts = findViewById(R.id.listview_contacts);

        if (!ContactsManager.INSTANCE.hasContacts()){
            TextView textViewMessage = findViewById(R.id.status_message);
            textViewMessage.setText(R.string.nocontacts);
            listViewContacts.setEmptyView(textViewMessage);
            return;
        }

        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, ContactsManager.INSTANCE.getContacts());

        listViewContacts.setAdapter(adapter);

        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(ContactDetailsActivity.getIntent(ContactsListActivity.this, i));
            }
        });
    }

    public static Intent getIntent(Context context){
        return new Intent(context, ContactsListActivity.class);
    }
}
