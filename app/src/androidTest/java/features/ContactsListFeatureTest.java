package features;

import android.support.test.rule.ActivityTestRule;

import com.example.carolinamarques.myapplication.ContactsListActivity;
import com.example.carolinamarques.myapplication.model.Contact;
import com.example.carolinamarques.myapplication.model.ContactsManager;
import com.mauriciotogneri.greencoffee.GreenCoffeeConfig;
import com.mauriciotogneri.greencoffee.GreenCoffeeTest;
import com.mauriciotogneri.greencoffee.Scenario;
import com.mauriciotogneri.greencoffee.ScenarioConfig;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Locale;

import steps.OpenContactsListFeatureSteps;

public class ContactsListFeatureTest extends GreenCoffeeTest {
    public ContactsListFeatureTest(ScenarioConfig scenario) {
        super(scenario);
    }

    @Rule
    public ActivityTestRule<ContactsListActivity> activity = new ActivityTestRule<>(ContactsListActivity.class);

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<ScenarioConfig> data() throws IOException {
        return new GreenCoffeeConfig().withFeatureFromAssets("assets/features/contactslist.feature").scenarios();
    }

    @Test
    public void test() {
        start(new OpenContactsListFeatureSteps());
    }

    @Override
    protected void beforeScenarioStarts(Scenario scenario, Locale locale) {
        ContactsManager.INSTANCE.clearAllContacts();
        ContactsManager.INSTANCE.add(new Contact("First Name"));
        ContactsManager.INSTANCE.add(new Contact("Second Name"));
    }
}
