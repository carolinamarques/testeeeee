package steps;

import com.example.carolinamarques.myapplication.R;
import com.example.carolinamarques.myapplication.model.Contact;
import com.example.carolinamarques.myapplication.model.ContactsManager;
import com.mauriciotogneri.greencoffee.GreenCoffeeSteps;
import com.mauriciotogneri.greencoffee.annotations.Given;

import static org.junit.Assert.assertTrue;

public class OpenContactsListFeatureSteps extends GreenCoffeeSteps {

    @Given("^I have opened the application$")
    public void i_have_oppened_theapplication(){
        assertTrue("Contacts".equalsIgnoreCase(string(R.string.app_name)));
        onViewWithText(string(R.string.app_name)).isDisplayed();
    }

    @Given("^I have contacts$")
    public void i_have_contacts(){
        assertTrue(ContactsManager.INSTANCE.hasContacts());
    }

    @Given("^I see the list of contacts' names$")
    public void i_see_the_list_of_contacts_names(){
        for(Contact contact : ContactsManager.INSTANCE.getContacts()){
            onViewWithText(contact.getName()).isDisplayed();
            onViewWithObject(contact).isDisplayed();
        }
    }

}
