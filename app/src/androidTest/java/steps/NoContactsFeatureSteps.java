package steps;

import com.example.carolinamarques.myapplication.R;
import com.example.carolinamarques.myapplication.model.ContactsManager;
import com.mauriciotogneri.greencoffee.GreenCoffeeSteps;
import com.mauriciotogneri.greencoffee.annotations.Given;
import com.mauriciotogneri.greencoffee.annotations.Then;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NoContactsFeatureSteps extends GreenCoffeeSteps {

    @Given("^I have opened the application$")
    public void i_have_opened_the_application() {
        // Write code here that turns the phrase above into concrete actions
        assertTrue("Contacts".equalsIgnoreCase(string(R.string.app_name)));
        onViewWithText(string(R.string.app_name)).isDisplayed();
    }

    @Given("^I have no contacts$")
    public void i_have_no_contacts() {
        // Write code here that turns the phrase above into concrete actions
        assertFalse(ContactsManager.INSTANCE.hasContacts());
    }

    @Then("^I see an empty contact list$")
    public void i_see_an_empty_contact_list() {
        // Write code here that turns the phrase above into concrete actions
        onViewWithId(R.id.listview_contacts).isNotDisplayed();

    }

    @Then("^I see a message saying that I have no contacts$")
    public void i_see_a_message_saying_that_I_have_no_contacts() {
        // Write code here that turns the phrase above into concrete actions
        onViewWithId(R.id.status_message).contains(string(R.string.nocontacts));
    }
}
